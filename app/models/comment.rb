# Comment class now has all the methods of the Base class
# And a post object can use comment methods 
class Comment < ActiveRecord::Base
  belongs_to :post # association of post to many comments
  validates_presence_of :post_id
  validates_presence_of :body
end  # dependent:  :destroy is when a post is destroyed, so will its comments
