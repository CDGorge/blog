class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy
  validates_presence_of :title # throws error if title isn't given in a post
  validates_presence_of :body
end
